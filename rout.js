const express = require('express')
const app = express()

app.get('/', (req, res)=> {
	res.send('Hi There, Welcome');
});

app.get('/speak/:animal', (req, res)=> {
	console.log(req.params);
	var animal = req.params.animal;
	if(animal==='cat')
	res.send('Moew');
	else if (animal==='cow')
	res.send('muu');
	else if (animal==='dog')
	res.send('woof woof')
});

app.get('/repeat/hello/:ile', function (req, res){
	
	var sIle = req.params.ile; // string ile
	var ile = parseInt(sIle, 10); // int
	var string1 = "";
	
	for (var i=0; i<ile; i++)
		{
			string1 = string1 +  " hello ";
		}
	
	res.send(string1);
	
});

app.get('*', (req, res)=> {
	res.send('Soooo sorry');
});


app.listen(3000, ()=> {
 	console.log('serwer slucha na 3000');	
});